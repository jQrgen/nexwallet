pluginManagement {
    repositories {
        google()
        mavenLocal()
        mavenCentral()
        maven { url = uri("https://gitlab.com/api/v4/projects/48544966/packages/maven") }  // mpThreads
        maven { url = uri("https://gitlab.com/api/v4/projects/48545045/packages/maven") } // LibNexaKotlin
        gradlePluginPortal()
    }
}

include(":app", ":shared"/*, ":libnexakotlin", ":libnexa"*/)
rootProject.name = "NexaWallet"
// project(":libnexakotlin").projectDir = rootDir.resolve("../libnexakotlin")
// project(":libnexa").projectDir = rootDir.resolve("../libnexakotlin/libnexa")