package info.bitcoinunlimited.nexawallet.android

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import info.bitcoinunlimited.nexawallet.AppInfo
import info.bitcoinunlimited.nexawallet.initKoin
import org.koin.dsl.module

class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin(
            module {
                single<Context> { this@MainApp }
                single<SharedPreferences> {
                    get<Context>().getSharedPreferences("KAMPSTARTER_SETTINGS", MODE_PRIVATE)
                }
                single<AppInfo> { AndroidAppInfo }
                single {
                    { Log.i("Startup", "Hello from Android/Kotlin!") }
                }
            }
        )
    }
}

object AndroidAppInfo : AppInfo {
    // TODO: Unresolved reference: BuildConfig
    override val appId: String = "BuildConfig.APPLICATION_ID"
}
