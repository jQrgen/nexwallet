

plugins {
    kotlin("multiplatform")
    kotlin("native.cocoapods")
    kotlin("plugin.serialization")
    id("com.android.library")
    id("org.jetbrains.compose")
    id("dev.icerock.mobile.multiplatform-resources")
    id("org.jetbrains.dokka")
}

android {
    namespace = "info.bitcoinunlimited.nexawallet"
    compileSdk = libs.versions.compileSdk.get().toInt()
    defaultConfig {
        minSdk = libs.versions.minSdk.get().toInt()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    testOptions {
        unitTests {
            isIncludeAndroidResources = true
        }
    }

    lint {
        warningsAsErrors = true
        abortOnError = true
    }
}

version = "1.2"

kotlin {
    androidTarget()
    ios {
        binaries {
            findTest(org.jetbrains.kotlin.gradle.plugin.mpp.NativeBuildType.DEBUG)?.linkerOpts("-lsqlite3")
        }
    }
    // Note: iosSimulatorArm64 target requires that all dependencies have M1 support
    iosSimulatorArm64 {
        binaries {
            findTest(org.jetbrains.kotlin.gradle.plugin.mpp.NativeBuildType.DEBUG)?.linkerOpts("-lsqlite3")
        }
    }

    sourceSets {
        all {
            languageSettings.apply {
                optIn("kotlin.RequiresOptIn")
                optIn("kotlinx.coroutines.ExperimentalCoroutinesApi")
            }
        }

        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib"))
                implementation(libs.libnexakotlin) // Release
                // implementation(project(":libnexakotlin")) // Debug
                implementation(compose.runtime)
                implementation(compose.foundation)
                implementation(compose.material) // for PullRefreshIndicator
                implementation(compose.material3)
                implementation(compose.materialIconsExtended)
                implementation(libs.koin.core)
                implementation(libs.coroutines.core)
                implementation(libs.bundles.ktor.common)
                implementation(libs.multiplatformSettings.common)
                implementation(libs.kotlinx.dateTime)
                api(libs.touchlab.kermit)
                api(libs.moko.resources)
                api(libs.moko.resources.compose)
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(libs.bundles.shared.commonTest)
            }
        }
        val androidMain by getting {
            // Below line adds a temporary workaround for https://github.com/icerockdev/moko-resources/issues/531
            kotlin.srcDirs("build/generated/moko/androidMain/src")
            dependencies {
                implementation(libs.compose.activity)
                implementation(libs.androidx.lifecycle.viewmodel)
                implementation(libs.ktor.client.okHttp)
            }
        }
        val androidInstrumentedTest by getting {
            dependencies {
                implementation(libs.bundles.shared.androidTest)
            }
        }
        val iosMain by getting {
            resources.srcDirs("build/generated/moko/iosMain/src")
            dependencies {
                implementation(libs.ktor.client.ios)
            }
        }
        val iosTest by getting
        val iosSimulatorArm64Main by getting {
            resources.srcDirs("build/generated/moko/iosSimulatorArm64Main/src")
            dependsOn(iosMain)
        }
        val iosSimulatorArm64Test by getting {
            dependsOn(iosTest)
        }
    }

    sourceSets.matching { it.name.endsWith("Test") }
        .configureEach {
            languageSettings.optIn("kotlin.time.ExperimentalTime")
        }

    sourceSets.matching { it.name.endsWith("TestDebug") }
        .configureEach {
            languageSettings.optIn("kotlin.time.ExperimentalTime")
        }

    sourceSets.matching { it.name.endsWith("TestRelease") }
        .configureEach {
            languageSettings.optIn("kotlin.time.ExperimentalTime")
        }

    cocoapods {
        summary = "Common library for the Nexa Wallet"
        homepage = "https://gitlab.com/jQrgen/nexawallet"
        framework {
            isStatic = false // SwiftUI preview requires dynamic framework
            linkerOpts("-lsqlite3")
            // TODO: Export kermit?
            // export(libs.touchlab.kermit.simple)
            // TODO: Tie moko versions to libs.versions
            export("dev.icerock.moko:resources:0.23.0")
            export("dev.icerock.moko:graphics:0.9.0") // toUIColor here
        }
        ios.deploymentTarget = "14.0"
        podfile = project.file("../ios/Podfile")
    }
}

multiplatformResources {
    multiplatformResourcesPackage = "info.bitcoinunlimited.nexawallet" // required
    multiplatformResourcesClassName = "MR" // optional, default MR
    multiplatformResourcesVisibility = dev.icerock.gradle.MRVisibility.Internal // optional, default Public
    iosBaseLocalizationRegion = "en" // optional, default "en"
    multiplatformResourcesSourceSet = "commonMain" // optional, default "commonMain"
}

// Fixes crash in runKtlintCheckOverAndroidMainSourceSet
tasks.named("runKtlintCheckOverAndroidMainSourceSet") {
    dependsOn(":shared:generateMRandroidMain")
}

// Fixes build error in runKtlintCheckOverCommonMainSourceSet
tasks.named("runKtlintCheckOverCommonMainSourceSet") {
    dependsOn(":shared:generateMRcommonMain")
}
