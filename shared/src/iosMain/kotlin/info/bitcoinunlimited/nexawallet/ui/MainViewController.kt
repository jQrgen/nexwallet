package info.bitcoinunlimited.nexawallet.ui

import androidx.compose.ui.window.ComposeUIViewController
import info.bitcoinunlimited.nexawallet.models.WalletViewModel
import info.bitcoinunlimited.nexawallet.ui.theme.NexaTheme
import co.touchlab.kermit.Logger
import platform.UIKit.UIScreen
import platform.UIKit.UIUserInterfaceStyle

fun MainViewController(logger: Logger, viewModel: WalletViewModel) = ComposeUIViewController {
    val isDarkThemeEnabled = UIScreen.mainScreen.traitCollection.userInterfaceStyle == UIUserInterfaceStyle.UIUserInterfaceStyleDark

    NexaTheme(isDarkThemeEnabled, false) {
        MainScreen(viewModel, logger)
    }
}
