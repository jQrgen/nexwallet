package info.bitcoinunlimited.nexawallet

import co.touchlab.kermit.Logger
import info.bitcoinunlimited.nexawallet.models.CallbackViewModel
import info.bitcoinunlimited.nexawallet.models.WalletRepository
import info.bitcoinunlimited.nexawallet.models.WalletViewModel

@Suppress("Unused") // Members are called from Swift
class WalletCallbackViewModel(
    breedRepository: WalletRepository,
    log: Logger
) : CallbackViewModel() {
    override val viewModel = WalletViewModel(breedRepository, log)

    val address = viewModel.walletState.asCallbacks()
}
