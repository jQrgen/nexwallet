package info.bitcoinunlimited.nexawallet.models

import co.touchlab.kermit.Logger
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import org.nexa.libnexakotlin.ChainSelector
import org.nexa.libnexakotlin.GetBlockchain
import org.nexa.libnexakotlin.GetCnxnMgr
import org.nexa.libnexakotlin.LibNexa

class WalletRepository(
    private val log: Logger,
    @Suppress("unused")
    private val libNexa: LibNexa
) {
    private val chainSelector: ChainSelector = ChainSelector.NEXA
    private val mutableAddressState: MutableStateFlow<String?> = MutableStateFlow(null)
    val addressState: StateFlow<String?> = mutableAddressState

    fun initWallet() {
        log.d("initWallet")
        val cnxnMgr = GetCnxnMgr(chainSelector)
        log.d("CnxnMgr created: $cnxnMgr")

        cnxnMgr.changeCallback = { cnxnMgr, p2pClient ->
            log.d("CnxnMgr changed")
            log.d(cnxnMgr.toString())
            log.d(p2pClient.toString())
            log.d("p2pClient.size: ${cnxnMgr.size}")
        }
        val blockchain = GetBlockchain(chainSelector, cnxnMgr)
        log.d("Blockchain created: ${blockchain.name}")
    }

    suspend fun getNewAddress(): String {
        return "mock:addressaddressaddressaddressaddress"
        // TODO("Implement")
    }

    fun getMnemonic(): String {
        return "TODO: Implement"
        // TODO("Implement")
    }
}
