package info.bitcoinunlimited.nexawallet.models

import co.touchlab.kermit.Logger
import info.bitcoinunlimited.nexawallet.ui.NexaTx
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

data class WalletViewState(
    val address: String? = null,
    val transactions: List<NexaTx> = listOf(),
    val error: String? = null,
    val isLoading: Boolean = false,
    val isEmpty: Boolean = false
)

class WalletViewModel(
    private val walletRepository: WalletRepository,
    log: Logger
) : ViewModel() {
    private val log = log.withTag("WalletViewModel")
    private val mutableWalletState: MutableStateFlow<WalletViewState> =
        MutableStateFlow(WalletViewState(isLoading = true))
    val walletState: StateFlow<WalletViewState> = mutableWalletState

    private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        // Handle your exception here
        log.e("WalletViewModel CoroutineExceptionHandler")
        log.e("Caught $exception")
    }

    override fun onCleared() {
        log.v("Clearing WalletViewModel")
    }

    init {
        walletRepository.initWallet()
        initWallet()
    }

    private fun initWallet() {
        viewModelScope.launch(exceptionHandler) {
            walletRepository.addressState.collect {
                mutableWalletState.value = WalletViewState(
                    address = it ?: "loading..."
                )
            }
        }
        // TODO: Use supervisorJob to handle coroutines here?
        viewModelScope.launch(Dispatchers.IO + exceptionHandler) {
            walletRepository.getNewAddress()
        }
    }
}
