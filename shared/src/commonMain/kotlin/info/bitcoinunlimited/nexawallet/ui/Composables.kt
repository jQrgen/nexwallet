package info.bitcoinunlimited.nexawallet.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import info.bitcoinunlimited.nexawallet.models.WalletViewModel
import info.bitcoinunlimited.nexawallet.models.WalletViewState
import co.touchlab.kermit.Logger

@Composable
fun MainScreen(
    viewModel: WalletViewModel,
    log: Logger
) {
    val walletState by viewModel.walletState.collectAsState()
    val scope = rememberCoroutineScope()

    Text("Hello from compose")
    /*

    // TODO: Implement MainScreenContent
    MainScreenContent(
        walletState = walletState,
        onRefresh = {
            scope.launch {
                // TODO: viewModel.refreshTransactions
            }
        }
        // TODO: Implement parameters
    )
     */
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun MainScreenContent(
    walletState: WalletViewState,
    onRefresh: () -> Unit = {}
) {
    Surface(
        color = MaterialTheme.colors.background,
        modifier = Modifier.fillMaxSize()
    ) {
        // TODO: Implement
        val refreshState = rememberPullRefreshState(walletState.isLoading, onRefresh)

        Box(Modifier.pullRefresh(refreshState)) {
            if (walletState.isEmpty) {
                Empty()
            }
        }
    }
}

@Composable
fun Empty() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        // TODO: Add stringResource
        // Text(text = stringResource(MR.strings.empty_breeds))
        Text(text = "Hello world kotlin compose multiplatform")
    }
}
data class NexaTx(
    val txid: String,
    val name: String
)
val mockNexaTxes = listOf(
    NexaTx(txid = "0", name = "appenzeller"),
    NexaTx(txid = "1", name = "australian"),
    NexaTx(txid = "2", name = "appenzeller"),
    NexaTx(txid = "3", name = "australian"),
    NexaTx(txid = "4", name = "appenzeller"),
    NexaTx(txid = "5", name = "australian"),
    NexaTx(txid = "6", name = "appenzeller"),
    NexaTx(txid = "7", name = "australian"),
    NexaTx(txid = "8", name = "appenzeller"),
    NexaTx(txid = "9", name = "australian"),
    NexaTx(txid = "10", name = "appenzeller"),
    NexaTx(txid = "11", name = "australian"),
    NexaTx(txid = "12", name = "appenzeller"),
    NexaTx(txid = "13", name = "australian"),
    NexaTx(txid = "14", name = "appenzeller"),
    NexaTx(txid = "15", name = "australian"),
    NexaTx(txid = "16", name = "appenzeller"),
    NexaTx(txid = "17", name = "australian"),
    NexaTx(txid = "18", name = "appenzeller"),
    NexaTx(txid = "19", name = "australian"),
    NexaTx(txid = "20", name = "appenzeller"),
    NexaTx(txid = "21", name = "australian"),
    NexaTx(txid = "22", name = "appenzeller"),
    NexaTx(txid = "23", name = "australian"),
    NexaTx(txid = "24", name = "appenzeller"),
    NexaTx(txid = "25", name = "australian"),
    NexaTx(txid = "26", name = "appenzeller"),
    NexaTx(txid = "27", name = "australian"),
    NexaTx(txid = "28", name = "appenzeller"),
    NexaTx(txid = "29", name = "australian")
)
