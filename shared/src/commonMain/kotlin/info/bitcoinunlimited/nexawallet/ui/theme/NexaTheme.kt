package info.bitcoinunlimited.nexawallet.ui.theme

import androidx.compose.runtime.Composable

@Composable
expect fun NexaTheme(
    darkTheme: Boolean,
    dynamicColor: Boolean,
    content: @Composable () -> Unit
)
