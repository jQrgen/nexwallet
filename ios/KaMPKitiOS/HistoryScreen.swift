//
//  HistoryScreen.swift
//  KaMPKitiOS
//
//  Created by Jørgen Svennevik Notland on 09/08/2023.
//  Copyright © 2023 Touchlab. All rights reserved.
//

import SwiftUI

struct NexaTx: Codable {
    let txid: String
    let name: String
}

let mockNexaTxes = [
    NexaTx(txid: "0", name: "appenzeller"),
    NexaTx(txid: "1", name: "australian"),
    NexaTx(txid: "2", name: "appenzeller"),
    NexaTx(txid: "3", name: "australian"),
    NexaTx(txid: "4", name: "appenzeller"),
    NexaTx(txid: "5", name: "australian"),
    NexaTx(txid: "6", name: "appenzeller"),
    NexaTx(txid: "7", name: "australian"),
    NexaTx(txid: "8", name: "appenzeller"),
    NexaTx(txid: "9", name: "australian"),
    NexaTx(txid: "10", name: "appenzeller"),
    NexaTx(txid: "11", name: "australian"),
    NexaTx(txid: "12", name: "appenzeller"),
    NexaTx(txid: "13", name: "australian"),
    NexaTx(txid: "14", name: "appenzeller"),
    NexaTx(txid: "15", name: "australian"),
    NexaTx(txid: "16", name: "appenzeller"),
    NexaTx(txid: "17", name: "australian"),
    NexaTx(txid: "18", name: "appenzeller"),
    NexaTx(txid: "19", name: "australian"),
    NexaTx(txid: "20", name: "appenzeller"),
    NexaTx(txid: "21", name: "australian"),
    NexaTx(txid: "22", name: "appenzeller"),
    NexaTx(txid: "23", name: "australian"),
    NexaTx(txid: "24", name: "appenzeller"),
    NexaTx(txid: "25", name: "australian"),
    NexaTx(txid: "26", name: "appenzeller"),
    NexaTx(txid: "27", name: "australian"),
    NexaTx(txid: "28", name: "appenzeller"),
    NexaTx(txid: "29", name: "australian")
]


struct HistoryScreen: View {
    var body: some View {
        HistoryContent(
            loading: false,
            transactions: mockNexaTxes,
            error: nil, // TODO: Implement
            refresh: { } // TODO: Implement
        )
    }
}

struct HistoryContent: View {
    var loading: Bool
    var transactions: [NexaTx]?
    var error: String?
    var refresh: () -> Void

    var body: some View {
        ZStack {
            VStack {
                if let transactions = transactions {
                    List(transactions, id: \.txid) { transaction in
                        TransactionRowView(transaction: transaction) {
                        }
                    }
                }
                if let error = error {
                    Text(error)
                        .foregroundColor(.red)
                }
                Button("Refresh") {
                    refresh()
                }
            }
            if loading { Text("Loading...") }
        }
    }
}

struct TransactionRowView: View {
    var transaction: NexaTx
    var onTap: () -> Void

    var body: some View {
        Button(action: onTap) {
            HStack {
                Text(transaction.name)
                    .padding(4.0)
                Spacer()
            }
        }
    }
}

struct HistoryScreen_Previews: PreviewProvider {
    static var previews: some View {
        HistoryContent(
            loading: false,
            transactions: mockNexaTxes,
            error: nil,
            refresh: {}
        )
    }
}
