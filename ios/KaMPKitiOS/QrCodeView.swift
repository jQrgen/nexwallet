//
//  QrCodeView.swift
//  KaMPKitiOS
//
//  Created by Jørgen Svennevik Notland on 26/05/2023.
//  Copyright © 2023 Touchlab. All rights reserved.
//

import SwiftUI
import CoreImage.CIFilterBuiltins

struct QrCodeView: View {
    let context = CIContext()
    let filter = CIFilter.qrCodeGenerator()
    var url: String

    var body: some View {
        Image(uiImage: generateQRCode(url: url))
            .interpolation(.none)
            .resizable()
            .scaledToFit()
            .frame(width: 200, height: 200)
    }

    func generateQRCode(url: String) -> UIImage {
        let data = Data(url.utf8)
        filter.setValue(data, forKey: "inputMessage")

        if let qrCodeImage = filter.outputImage {
            if let qrCodeCGImage = context.createCGImage(qrCodeImage, from: qrCodeImage.extent) {
                return UIImage(cgImage: qrCodeCGImage)
            }
        }

        return UIImage(systemName: "xmark.circle") ?? UIImage()
    }
}

struct QrCodeView_Previews: PreviewProvider {
    static var previews: some View {
        QrCodeView(url: "nrk.no")
    }
}
