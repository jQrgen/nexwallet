//
//  SettingsScreen.swift
//  KaMPKitiOS
//
//  Created by Jørgen Svennevik Notland on 11/08/2023.
//  Copyright © 2023 Touchlab. All rights reserved.
//

import LocalAuthentication
import SwiftUI
private let log = koin.loggerWithTag(tag: "SettingsScreen")

struct SettingsScreen: View {
    @State private var isUnlocked = false
    @State private var failedAuth = ""

    private func authenticate() {
            var error: NSError?
            let laContext = LAContext()

            if laContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                let reason = "Need access to authenticate"

                laContext.evaluatePolicy(
                    .deviceOwnerAuthenticationWithBiometrics,
                     localizedReason: reason) { success, error in
                    DispatchQueue.main.async {
                        if success {
                            log.d(message: {"faceid success"})
                            isUnlocked = true
                            failedAuth = "yay"
                        } else {
                            log.e(message: {"Displaying error: \(error?.localizedDescription ?? "error")"})

                            log.e(message: {error?.localizedDescription ?? "error"})
                            failedAuth = error?.localizedDescription ?? "error"
                        }
                    }
                }
            } else {
                log.e(message: {"feil"})
            }
        }
    var body: some View {
        Button(action: authenticate) {
            Label("Login with FaceID", systemImage: "faceid")
                                    .padding()
                                    .background(RoundedRectangle(cornerRadius: 15).foregroundColor(.white))
        }
        Text(failedAuth)
            .padding()
            .opacity(failedAuth.isEmpty ? 0.0 : 1.0)
        // TODO: Display disable faceid/fingerprint
        // lock button when enabled
    }
}

struct SettingsScreen_Previews: PreviewProvider {
    static var previews: some View {
        SettingsScreen()
    }
}
