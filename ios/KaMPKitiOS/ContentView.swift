//
//  ContentView.swift
//  KaMPKitiOS
//
//  Created by Jørgen Svennevik Notland on 28/08/2023.
//  Copyright © 2023 Touchlab. All rights reserved.
//

import SwiftUI
import shared

struct ContentView: View {
    var body: some View {
        ComposeContentView()
    }
}
