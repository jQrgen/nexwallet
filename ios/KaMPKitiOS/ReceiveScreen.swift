//
//  WalletScreen.swift
//  KaMPKitiOS
//
//  Created by Jørgen Svennevik Notland on 30/01/2023.
//  Copyright © 2023 Touchlab. All rights reserved.
//

import Foundation

import Combine
import SwiftUI
import shared

private let log = koin.loggerWithTag(tag: "WalletViewController")

class ObservableWalletModel: ObservableObject {
    private var viewModel: WalletCallbackViewModel?

    @Published
    var loading = false

    @Published
    var address: String?

    @Published
    var error: String?

    private var cancellables = [AnyCancellable]()

    func activate() {
        let viewModel = KotlinDependencies.shared.getWalletViewModel()

        doPublish(viewModel.address) { [weak self] wallet in
            self?.loading = wallet.isLoading
            self?.address = wallet.address
            self?.error = wallet.error

            if let address = wallet.address {
                log.d(message: {"View updating with \(address)"})
            }
            if let errorMessage = wallet.error {
                log.e(message: {"Displaying error: \(errorMessage)"})
            }
        }.store(in: &cancellables)

        self.viewModel = viewModel
    }

    func deactivate() {
        cancellables.forEach { $0.cancel() }
        cancellables.removeAll()

        viewModel?.clear()
        viewModel = nil
    }
}

struct ReceiveScreen: View {
    @StateObject
    var observableModel = ObservableWalletModel()

    var body: some View {
        ReceiveContent(
            loading: observableModel.loading,
            address: observableModel.address,
            error: observableModel.error
        )
        .onAppear(perform: {
            observableModel.activate()
        })
        .onDisappear(perform: {
            observableModel.deactivate()
        })
    }
}

struct ReceiveContent: View {
    @State private var showShareSheet = false
    var loading: Bool
    var address: String?
    var error: String?

    var body: some View {

        ZStack {
            if loading { Text("Loading...") }
            VStack {
                if let address = address {
                    QrCodeView(url: address)
                        .padding()
                    Text(address)
                        .padding()
                }
                if let error = error {
                    Text(error)
                        .foregroundColor(.red)
                }

                Button(action: {
                    let pasteboard = UIPasteboard.general
                    pasteboard.string = address
                }) {
                    Label("Copy address to clipboard", systemImage: "doc.on.doc")
                }
                .padding()
                
                Button(action: {
                    let activityController = UIActivityViewController(
                        activityItems: [address],
                        applicationActivities: nil
                    )
                    UIApplication.shared.windows.first?.rootViewController?.present(
                        activityController,
                        animated: true,
                        completion: nil
                    )
                }) {
                    Label("Share address", systemImage: "square.and.arrow.up")
                }
                .padding()
            }
        }
    }
}

struct ActivityView: UIViewControllerRepresentable {
    let activityItems: [Any]
    let applicationActivities: [UIActivity]? = nil

    func makeUIViewController(
        context: UIViewControllerRepresentableContext<ActivityView>
    ) -> UIActivityViewController {
        let controller = UIActivityViewController(
            activityItems: activityItems,
            applicationActivities: applicationActivities
        )
        return controller
    }

    func updateUIViewController(
        _ uiViewController: UIActivityViewController,
        context: UIViewControllerRepresentableContext<ActivityView>
    ) {

    }
}

struct SendScreen_Previews: PreviewProvider {
    static var previews: some View {
        ReceiveScreen()
    }
}
