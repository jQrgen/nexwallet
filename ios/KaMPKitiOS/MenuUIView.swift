//
//  MenuUIView.swift
//  KaMPKitiOS
//
//  Created by Jørgen Svennevik Notland on 26/05/2023.
//  Copyright © 2023 Touchlab. All rights reserved.
//

import LocalAuthentication
import SwiftUI

struct MenuUIView: View {
    let address: URL?
    @State private var selectedTab: Int
    @State private var isUnlocked = false
    @State private var failedAuth = ""
    @State private var isMnemonicScreenPresented = false
    @AppStorage("faceIDLockEnabled") var faceIDLockEnabled: Bool = false

    init(address: URL?) {
        self.address = address
        _selectedTab = State(initialValue: address == nil ? 0 : 1)
    }

    var body: some View {
        if(!isUnlocked && faceIDLockEnabled) {
            Button(action: authenticate ) {
                Label("Unlock with FaceID", systemImage: "faceid")
                    .padding()
                    .background(RoundedRectangle(cornerRadius: 15).foregroundColor(.white))
            }.onAppear {
                authenticate()
            }
            Text(failedAuth)
                .padding()
                .opacity(failedAuth.isEmpty ? 0.0 : 1.0)
        } else {
            VStack {
                Text("0 NEX") // TODO: Display balance here
                TabView(selection: $selectedTab) {
                    ReceiveScreen()
                        .tabItem {
                            Label("Receive", systemImage: "tray.and.arrow.down")
                        }
                        .tag(0)
                    SendScreen(address: address?.absoluteString ?? "")
                        .tabItem {
                            Label("Send", systemImage: "arrow.up.forward")
                        }
                        .tag(1)
                    HistoryScreen()
                        .tabItem {
                            Label("History", systemImage: "list.bullet")
                        }
                        .tag(2)
                    ContentView()
                        .tabItem {
                            Label("Compose", systemImage: "flame.fill")
                        }
                    NavigationView {
                        Form {
                            Toggle(isOn: $faceIDLockEnabled) {
                                Label("Face ID app lock", systemImage: "faceid")
                            }
                            Button("TBA: View 12 word mnemonic") {
                                isMnemonicScreenPresented = true
                                // TODO: Implement display mnemonic
                            }
                            Button("TBA: Recover from 12 word mnemonic") {
                                // TODO: Implement recovery
                            }
                            Link("Nexa website", destination: URL(string: "https://www.nexa.org")!)
                                .foregroundColor(.blue)
                            Text("Created by Bitcoin Unlimited")
                        }
                        .sheet(isPresented: $isMnemonicScreenPresented) {
                            Text("TODO: Display 12 word mnemonic here mnemonic mnemonic mnemonic mnemonic mnemonic mnemonic  ...")                    }
                    }
                        .tabItem {
                            Label("More", systemImage: "ellipsis.circle")
                        }
                        .tag(4)
                }
            }

        }
    }

    private func authenticate() {
            var error: NSError?
            let laContext = LAContext()

            if laContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                let reason = "Need access to authenticate"

                laContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, error in
                    DispatchQueue.main.async {
                        if success {
                            isUnlocked = true
                            failedAuth = ""
                        } else {
                            print(error?.localizedDescription ?? "error")
                            failedAuth = error?.localizedDescription ?? "error"
                        }
                    }
                }
            } else {
            }
        }
}

struct MenuUIView_Previews: PreviewProvider {
    static var previews: some View {
        MenuUIView(address: nil)
    }
}
