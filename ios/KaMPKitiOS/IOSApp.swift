//
//  IOSApp.swift
//  KaMPKitiOS
//
//  Created by Jørgen Svennevik Notland on 28/08/2023.
//  Copyright © 2023 Touchlab. All rights reserved.
//

import SwiftUI

@main
struct IOSApp: App {

    init() {
        startKoin()
    }

    var body: some Scene {
        WindowGroup {
            MenuUIView(address: URL(string: "mock:addressaddressaddressaddress"))
        }
    }
}
