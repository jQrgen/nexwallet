//
//  AppDelegate.swift
//  KaMPKitiOS
//
//  Created by Kevin Schildhorn on 12/18/19.
//  Copyright © 2019 Touchlab. All rights reserved.
//

import SwiftUI
import shared

// @UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // Lazy so it doesn't try to initialize before startKoin() is called
    lazy var log = koin.loggerWithTag(tag: "AppDelegate")
    var viewController: UIHostingController<MenuUIView>?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        startKoin()

        // Handle the situation where the app is launched in response to a deep link.
        if let url = launchOptions?[.url] as? URL {
            log.i(message: {"App started with deep link url: \(url.absoluteString)"})
            viewController = UIHostingController(rootView: MenuUIView(address: url))
        } else {
            viewController = UIHostingController(rootView: MenuUIView(address: nil))
        }

        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()

        log.v(message: {"App Started"})
        return true
    }

    func application(_ app: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        log.i(message: {"App opened with deep link url: \(url.absoluteString)"})

        // You can post a notification or communicate with a view model to handle the URL as needed
        // ...
        viewController = UIHostingController(rootView: MenuUIView(address: url))
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()

        return true
    }

}
