//
//  SendView.swift
//  KaMPKitiOS
//
//  Created by Jørgen Svennevik Notland on 09/08/2023.
//  Copyright © 2023 Touchlab. All rights reserved.
//

import CodeScanner
import SwiftUI

struct SendScreen: View {
    @State var address: String

    @StateObject
    var observableModel = ObservableWalletModel()

    var body: some View {
        SendContent(addr: $address)
            .onAppear(perform: {
                observableModel.activate()
            })
            .onDisappear(perform: {
                observableModel.deactivate()
            })
    }
}

struct SendContent: View {
    @Binding var addr: String
    @State private var nex = 0
    @State private var showingModal = false
    @State private var isShowingScanner = false

    let formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter
    }()

    var body: some View {
        Form {
            Section(header: Text("Nexa address")) {
                TextField("nexa:<hash>", text: $addr)
                    .autocapitalization(.none)
            }
            Section(header: Text("NEX to send")) {
                TextField("", value: $nex, formatter: formatter)
                    .keyboardType(.decimalPad)
            }
            Section {
                Button {
                    isShowingScanner = true
                } label: {
                    Label("Scan", systemImage: "qrcode.viewfinder")
                }
            }
            Section {
                Button("Send") {
                    showingModal.toggle()
                }
            }
        }.sheet(isPresented: $showingModal) {
            ModalView(addr: addr, nex: nex)
        }
        .sheet(isPresented: $isShowingScanner) {
            CodeScannerView(codeTypes: [.qr], simulatedData: "nexa:mockmocmockmockmockmock", completion: handleScan)
        }
    }

    func handleScan(result: Result<ScanResult, ScanError>) {
        isShowingScanner = false

        switch result {
        case.success(let result):
            addr = result.string

        case .failure(let error):
            print("Scanning failed: \(error)")
        }
    }
}

struct ModalView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var addr: String
    @State var nex: Int

    var body: some View {
        VStack {
            Text("Not Implemented")
            Text("Sending \(nex) NEX to address: \(addr) failed!")
            Button("Dismiss") {
                presentationMode.wrappedValue.dismiss()
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.white)
        .cornerRadius(10)
    }
}

struct SendView_Previews: PreviewProvider {
    @State private var text = "Hello, World!"
    static var previews: some View {
        SendContent(addr: .constant("nexa:mockmockmockmock"))
    }
}
